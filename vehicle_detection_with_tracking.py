import cv2
from ultralytics import YOLO
from sort import *
import cvzone
import math


model = YOLO("yolov8n.pt")
trackers = {}  
num = 0
CLASS_ID = [2, 3, 5, 7]


results = model("sources.streams", stream=True, classes=CLASS_ID)
saved_car_ids = set()

output_file = 'output_video.mp4'
codec = cv2.VideoWriter_fourcc(*'mp4v')

# Define the VideoWriter object
#out = cv2.VideoWriter(output_file, codec, 20, (1920, 1080))


for result in results:
    frame = result.orig_img.copy()
    height, width, _ = frame.shape

    mid_y = int(height / 2) 

    cv2.line(frame, (0, mid_y), (width, mid_y), (0,0,255), thickness=2)
    
    stream_name = result.path  

    if stream_name not in trackers:
        trackers[stream_name] = Sort(max_age=1, min_hits=1, iou_threshold=0.1) 

    detections = []
    for box in result.boxes:
        x1, y1, x2, y2 = box.xyxy[0]
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
        conf = math.ceil(box.conf[0] * 100) / 100

        class_id = int(box.cls)

        class_name = model.names[class_id]

        detections.append([x1, y1, x2, y2, conf])


        # cvzone.putTextRect(frame, f"Label: {class_name}", (x1, y1 - 10), scale=1, thickness=1, colorR=(0, 0, 255))
         # cvzone.cornerRect(frame, (x1, y1 , x2 - x1, y2 - y1 + 20), l=9, rt=1, colorR=(255, 0, 255))

    if len(detections) > 0:

        detections = np.array(detections)  

        tracker = trackers[stream_name]  
        tracked_objects = tracker.update(detections)

        for obj in tracked_objects:
            x1, y1, x2, y2, id = obj

            x1, y1, x2, y2, id = int(x1), int(y1), int(x2), int(y2), int(id)
            mid_y_car = (y1 + y2) // 2
            car_img = frame[y1:y2, x1:x2]

            if (mid_y_car - mid_y) > 0 and (mid_y_car - mid_y) < 100 and id not in saved_car_ids:
                              
                print(car_img.shape, "shape of car")
                if car_img.shape[0] > 270 and car_img.shape[1] > 270:
                    try:
                        num += 1
                        saved_car_ids.add(id)
                        cv2.imwrite(f"images/{num}_car.jpg", car_img)
                        # cv2.imwrite(f"images/{num}_screen.jpg", frame)
                    except:
                        pass

            cvzone.putTextRect(frame, f"ID: {id}", (x1, y1 - 10), scale=1, thickness=1, colorR=(0, 0, 255))
            cvzone.cornerRect(frame, (x1, y1 , x2 - x1, y2 - y1 + 20), l=9, rt=1, colorR=(255, 0, 255))


    cv2.imshow(result.path, frame)
    print(frame.shape)
    # out.write(frame)              
    if cv2.waitKey(1) == ord("q"):
        break

# out.release()
cv2.destroyAllWindows()
