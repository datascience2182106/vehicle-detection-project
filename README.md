# Vehicle Detection and Tracking with SORT Algorithm

This project implements a vehicle detection and tracking system using the Simple Online and Realtime Tracking (SORT) algorithm. It includes functionality to send information about detected vehicles to a server when they cross a predefined line.

## Overview

The system utilizes computer vision techniques to detect and track vehicles in a video stream. The SORT algorithm is employed for real-time multi-object tracking, allowing the system to monitor vehicles as they move across the screen. Additionally, a line is drawn on the screen to represent a virtual boundary or checkpoint. When a tracked vehicle crosses this line, its information is transmitted to a server for further processing.

## Key Features

- **Object Detection**: Vehicles are detected using a YOLO (You Only Look Once) object detection model, providing accurate and efficient detection in real-time.
- **SORT Tracking**: The SORT algorithm tracks the detected vehicles across frames, assigning unique IDs to each tracked object and maintaining their trajectories.
- **Line Crossing Detection**: A line is drawn on the screen to define a boundary. When a tracked vehicle crosses this line, its information is recorded and sent to a server.
- **Server Communication**: Information about vehicles that cross the line is transmitted to a server via HTTP requests, allowing for real-time monitoring and analysis.

## How It Works

1. **Object Detection and Tracking**:
    - The YOLO model detects vehicles in each frame of the video stream.
    - The SORT algorithm tracks the detected vehicles across frames, providing continuous updates on their positions and trajectories.

2. **Line Crossing Detection**:
    - A line is drawn on the screen to represent a virtual boundary or checkpoint.
    - As vehicles are tracked, their positions are compared to the line.
    - When a vehicle crosses the line, its information (ID, position, timestamp) is logged and sent to a server.

3. **Server Interaction**:
    - Upon detecting a vehicle crossing the line, its information is transmitted to a server.
    - The server can perform various actions based on the received data, such as logging events, updating a database, or triggering alerts.

## Setup and Usage

1. **Requirements**:
    - Python environment with necessary dependencies installed (OpenCV, SORT, etc.).
    - YOLO object detection model and weights.
    - Access to a server for receiving vehicle information.

2. **Configuration**:
    - Adjust parameters such as line position, server details, etc., as per your requirements.

3. **Running the System**:
    - Start the video stream or provide a video file as input.
    - Run the vehicle detection and tracking system.
    - Monitor the console for information about detected vehicles crossing the line.
    - Ensure the server receives and processes vehicle information correctly.

## Example Images

![vehicle_detection](detection_result.png)

Above is an example image showcasing the capabilities of the trained models in accurately detecting vehicle in real-time.

## Conclusion

The vehicle detection and tracking system with SORT algorithm provides an efficient solution for monitoring traffic flow and capturing events such as vehicles crossing specific points. By integrating with a server, the system enables real-time communication and data processing, facilitating various applications in traffic management, surveillance, and more.

